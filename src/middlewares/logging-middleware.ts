import { Request, Response} from "express"
import { Logger } from "../services/logger/logger";

export function createLoggingMiddleware(logger: Logger) {
  return function loggingMiddleware(req: Request, res: Response, next: any) {
    res.on("finish", () => {
      logger.info(`${req.method} ${res.statusCode} ${decodeURI(req.url)} ${res.statusMessage}`);
    });
    next();
  }
}

