import { Logger } from "./logger";

export class ConsoleLogger implements Logger {
  info(msg: string) {
    console.log(msg);
  }
}