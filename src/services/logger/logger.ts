export interface Logger {
  info(msg: string): void;
}