import {Express} from 'express';
import { createAppointmentsApi } from './appointments/appointments-api';
import { ProviderService } from './appointments/bl/provider-service';
import { AppointmentsService } from './appointments/bl/appointments-service';
import { ProvidersJsonStore } from './appointments/dal/providers-json-store';

export function createApi(app: Express) {
  const providersStore = new ProvidersJsonStore();
  const providersService = new ProviderService(providersStore);
  const appointmentsService = new AppointmentsService(providersService);

  app.use("/appointments", createAppointmentsApi(providersService, appointmentsService));
}