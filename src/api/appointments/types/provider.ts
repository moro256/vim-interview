export class Provider {
  public name: string;
  public score: number;
  public specialties: string[];
  public availableDates: AvailableDate[];
  
  constructor(providerRecord: ProviderRecord) {
    this.name = providerRecord.name;
    this.score = providerRecord.score;
    this.specialties = providerRecord.specialties;
    this.availableDates = providerRecord.availableDates;
  }

  public isDateAvailable(date: number) {
    return !!this.availableDates.find(availableDate => this.isDateRelevant(availableDate, date));
  }

  private isDateRelevant(date: AvailableDate, requestedDate: number) {
    return requestedDate >= date.from && requestedDate<=date.to;
  }
}


export interface ProviderRecord {
  name: string;
  score: number;
  specialties: string[];
  availableDates: AvailableDate[];
}

export interface AvailableDate {
  to: number;
  from: number;
}