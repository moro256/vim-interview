import { ProviderService } from "../bl/provider-service";
import {Request, Response} from "express";

export function getAppointments(req: Request, res: Response, providerService: ProviderService) {
  const specialty = req.query.specialty as string;
  const date = parseInt(req.query.date as string);
  const minScore = parseFloat(req.query.minScore as string);

  if (!specialty) {
    res.statusMessage = "specialty must be provided";
    res.status(400).end();
    return;
  }
  if (!date) {
    res.statusMessage = "date must be provided";
    res.status(400).end();
    return;
  }
  if (minScore === undefined) {
    res.statusMessage = "min score must be provided";
    res.status(400).end();
    return;
  }

  const providers = providerService.getProviderNames(specialty, minScore, date);
  res.send(providers);
}