import { AppointmentsService } from "../bl/appointments-service";
import {Request, Response} from "express";

export function postAppointment(req: Request, res: Response, appointmentsService: AppointmentsService) {
  const name = req.body.name as string;
  const date = req.body.date as number;
  if (!name) {
    res.statusMessage = "name must be provided";
    res.status(400).end();
    return;
  }

  if (!date) {
    res.statusMessage = "date must be provided";
    res.status(400).end();
    return;
  }

  const valid = appointmentsService.validateAppointment(name, date);
  if (!valid) {
    res.statusMessage = "appointment is not available";
    res.status(400).end();
    return;
  }

  res.status(200).end();
}