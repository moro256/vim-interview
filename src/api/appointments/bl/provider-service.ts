import { Provider } from "../types/provider";
import {type ProvidersStore} from "../dal/providers-store";

export class ProviderService {
  constructor(private ProvidersStore: ProvidersStore) {}

  getProviderByName(name: string): Provider {
    const record = this.ProvidersStore.getProviderByName(name);
    if (!record) {
      return null;
    }
    return new Provider(record);
  }

  getProviders(specialty: string, minScore: number, date: number): Provider[] {
    return this.ProvidersStore.getProviders(specialty, minScore, date)
      .map(record => new Provider(record));
  }

  getProviderNames(specialty: string, minScore: number, date: number): string[] {
    const relevantProviders =  this.getProviders(specialty, minScore, date);
    return relevantProviders.sort(provider => provider.score).reverse().map(provider => provider.name);
  }
}