import { ProviderService } from "./provider-service";

export class AppointmentsService {
  constructor(private providerService: ProviderService ) {}

  validateAppointment(name: string, date: number): boolean {
    const provider = this.providerService.getProviderByName(name);
    if (!provider) {
       return false;
    }

    return provider.isDateAvailable(date);
  }
}
