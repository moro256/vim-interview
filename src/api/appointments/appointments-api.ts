import express from 'express';
import { ProviderService } from './bl/provider-service';
import { AppointmentsService } from './bl/appointments-service';
import { getAppointments } from './controllers/get-appointments';
import { postAppointment } from './controllers/post-appointment';

export function createAppointmentsApi(providerService: ProviderService, appointmentsService: AppointmentsService) {
  const router = express.Router();

  router.get('/', (req, res) => getAppointments(req, res, providerService));
  router.post('/', (req, res) => postAppointment(req, res, appointmentsService));

  return router;
}
