import { AvailableDate, Provider, ProviderRecord } from "../types/provider";
import { ProvidersStore } from "./providers-store";

const providers = require("./providers.json");

export class ProvidersJsonStore implements ProvidersStore {
  private providers: ProviderRecord[];
  constructor() {
    this.providers = providers;
  }
  
  getAll() {
    return this.providers;
  }

  getProviders(specialty: string, minScore: number, date: number) {
    let providers = filterBySpecialty(this.providers, specialty);
    providers = filterByMinScore(providers, minScore);
    providers = filterByDate(providers, date);
    return providers;
  }

  getProviderByName(name: string) {
    return providers.find(provider => {
      return provider.name === name;
    });
  }
}


function filterBySpecialty(providers: ProviderRecord[], specialty: string) {
  return providers.filter(provider => {
    return !!provider.specialties.find(s => s.toLowerCase() === specialty.toLowerCase());
  });
}

function filterByMinScore(providers: ProviderRecord[], minScore: number) {
  return providers.filter(provider => {
    return provider.score >= minScore;
  });
}

function filterByDate(providers: ProviderRecord[], date: number) {
  return providers.filter(provider => {
    const p = new Provider(provider);
    return p.isDateAvailable(date);
  });
}