import { ProviderRecord } from "../types/provider";

export interface ProvidersStore {
  getAll(): ProviderRecord[];

  getProviders(specialty: string, minScore: number, date: number): ProviderRecord[];

  getProviderByName(name: string): ProviderRecord;
}