import express from 'express';
import { createApi } from './api/api-initializer';
import { createLoggingMiddleware } from './middlewares/logging-middleware';
import { ConsoleLogger } from './services/logger/console.logger';

export const app = express();
const port = 3500;

app.use(express.json());
app.use(createLoggingMiddleware(new ConsoleLogger()));
createApi(app);

app.listen(port, () => {
  return console.log(`Express is listening at http://localhost:${port}`);
});
